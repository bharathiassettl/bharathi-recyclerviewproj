import React, { Component } from 'react';
import { Text, View, Image, TouchableOpacity, StyleSheet } from 'react-native';
import TimeWidget from './TimeWidget';
import CardView from 'react-native-cardview';
export default class FlightCard extends Component {
  render() {
    return (
      // <TouchableOpacity onPress={() => alert("working")}>

      //   <View style={styles.outerContainer}>
      //     <View style={styles.container}>
      //       <Image
      //         style={styles.image}
      //         source={this.props.data.image}
      //       />
      //       {/* <TimeWidget data={this.props.data.values} /> */}
      //       <Text style={styles.finalPriceText}>
      //         {this.props.data.id}
      //       </Text>
      //     </View>

      //     <View style={styles.line}></View>
      //   </View>
      // </TouchableOpacity>
      <TouchableOpacity style={styles.CardHorizantalLL} onPress={() => this.showAlert(this.props.data.name)}>

        <View style={styles.CardLL}>
          <CardView
            cardElevation={5}
            cardMaxElevation={5}
            cornerRadius={5}
            style={styles.cardViewStyle}>
            <Image style={styles.cardInsideImage} source={this.props.data.image} />
            <Text style={styles.name}>{this.props.data.title}</Text>
            <Text style={styles.value}>{this.props.data.id}</Text>

          </CardView>
        </View>
      </TouchableOpacity>


    );
  }
}

// const styles = {
//   outerContainer: {
//     flex: 1,
//     alignItems: 'stretch',
//   },
//   container: {
//     flex: 1,
//     flexDirection: 'row',
//     justifyContent: 'space-between',
//     backgroundColor: 'white',
//     alignItems: 'center',
//     padding: 16,
//   },
//   finalPriceText: {
//     marginLeft: 16,
//     color: 'blue',
//     fontSize: 16,
//   },
//   image: {
//     height: 50,
//     width: 50,
//     marginRight: 16,
//   },
//   line: {
//     height: 1.5,
//     backgroundColor: '#d3d3d3',
//   },
//};
const styles = StyleSheet.create({

  outerContainer: {
    flex: 1,
    alignItems: 'stretch',
    marginStart: 20,
    marginEnd: 10,
    marginTop: 5,
    borderRadius: 10,
  },


  nameEmailView: {
    justifyContent: 'center',
    flexDirection: "column",
    justifyContent: "center",
    alignContent: "center",
    alignSelf: "flex-start",
    marginStart: 10,
  },
  name: {
    fontFamily: 'montserrat_medium',
    fontSize: 15,
    color: '#000000',
    alignItems: 'center',
    alignContent: 'center',
    justifyContent: 'center'
  },
  value: {
    color: '#000000',
    fontStyle: "normal",
    fontFamily: 'montserrat_medium',
    fontSize: 15,
  },
  cardViewStyle: {
    width: 150,
    height: 150,
    alignItems: "center"
  },
  cardInsideImage: {
    width: 50,
    height: 50,
    marginTop: 30,
    marginBottom: 5
  },
  CardLL: {
    flex: 1,
    marginLeft: 30,
    marginRight: 10,
    marginTop: 20,
    alignItems: "center"
  },
  CardHorizantalLL: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
  },
});

