import React, { Component } from 'react';

import { View, Dimensions, Text, Image } from 'react-native';
import { RecyclerListView, LayoutProvider, DataProvider } from 'recyclerlistview';
import FlightCard from './screen/FlightCard';
import FlightData from './screen/FlightData';
import HotelCard from './screen/HotelCard';
import TopWidget from './screen/TopWidget';
import http from 'axios';


let { height, width } = Dimensions.get('window');


// orientation must fixed
const SCREEN_WIDTH = width < height ? width : height;
// const SCREEN_HEIGHT = width < height ? height : width;
const isSmallDevice = SCREEN_WIDTH <= 400;
const numColumns = isSmallDevice ? 2 : 3;
// item size
const PRODUCT_ITEM_HEIGHT = 170;
const PRODUCT_ITEM_OFFSET = 5;
const PRODUCT_ITEM_MARGIN = PRODUCT_ITEM_OFFSET * 2;

export default class App extends Component {
  constructor(args) {
    super(args);
    this.state = {
      dataProvider: new DataProvider((r1, r2) => {
        return r1 !== r2;
      }),
    };
    this._layoutProvider = new LayoutProvider(
      (i) => {
        return this.state.dataProvider.getDataForIndex(i).id;
      },
      (id, dim) => {
        switch (id) {

          case 123:
            dim.width = width;
            dim.height = 80;
            break;

          default:
            dim.width = width / 2.125;
            dim.height = PRODUCT_ITEM_HEIGHT + PRODUCT_ITEM_MARGIN;
        }
      },
    );
    this._renderRow = this._renderRow.bind(this);
  }

  componentDidMount() {
    http
      .get('https://companypurpose.herokuapp.com/api/Maplatlng')
      .then((res) => {
        console.log(res.data);

        let dd = [];
        res.data.forEach((element) => {
          let s = {

            id: element.id,
            title: element.title,
            image: ""

          };
          if (element.title == "Idle") {
            s.image = require('./screen/Assets/asset_9.png');
          }
          else if (element.title == "Stopped") {
            s.image = require('./screen/Assets/asset_10.png');
          }
          else if (element.title == "All") {
            s.image = require('./screen/Assets/asset_11.png');

          }
          else if (element.title == "Moving") {
            s.image = require('./screen/Assets/asset_12.png');

          }
          else if (element.title == "No Gps") {
            s.image = require('./screen/Assets/asset_5.png');

          }

          else if (element.title == "Not Polled") {
            s.image = require('./screen/Assets/asset_6.png');

          }

          else {
            s.image = require('./screen/Assets/asset_8.png');
          }

          dd.push(s);
        });
        console.log(dd);

        this.setState({
          dataProvider: new DataProvider((r1, r2) => {
            return r1 !== r2;
          }).cloneWithRows(dd),
        });
      })
      .catch((err) => {
        console.log(err);
      });
  }

  _renderRow(id, data) {
    switch (id) {

      case 123:
        return <FlightCard data={data} />;

      default:
        return <FlightCard data={data} />;
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.header}>
          <Text style={styles.headerText}>RecyclerList view</Text>
        </View>
        <RecyclerListView
          rowRenderer={this._renderRow}
          dataProvider={this.state.dataProvider}
          layoutProvider={this._layoutProvider}
        />
      </View>
    );
  }
}
const styles = {
  container: {
    flex: 1,
  },
  header: {
    height: 65,
    backgroundColor: 'orange',
    alignItems: 'center',
    flexDirection: 'row',
    elevation: 4,
  },
  headerText: {
    color: 'white',
    fontSize: 18,
    marginLeft: 16,
    paddingBottom: 3,
  },
  backIcon: {
    height: 23,
    width: 23,
    marginLeft: 16,
  },
};

